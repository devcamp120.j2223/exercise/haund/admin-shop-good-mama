import React from 'react'
import CIcon from '@coreui/icons-react'
import { cilList, cilNotes, cibProductHunt, cilPeople } from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  // {
  //   component: CNavTitle,
  //   name: 'Shop24h',
  // },
  {
    component: CNavItem,
    name: 'Products',
    to: '/shop24h/product',
    icon: <CIcon icon={cibProductHunt} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'ProductType',
    to: '/shop24h/productTypes',
    icon: <CIcon icon={cilList} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Orders',
    to: '/shop24h/orders',
    icon: <CIcon icon={cilNotes} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Customers',
    to: '/shop24h/customers',
    icon: <CIcon icon={cilPeople} customClassName="nav-icon" />,
  },
]
export default _nav
