import React, { useEffect, useState } from 'react'
import { cilPen, cilDelete, cibAddthis } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
const ProductTypes = () => {
  const [productTypes, setProductTypes] = useState([])
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions)
    const responseData = await response.json()
    return responseData
  }
  useEffect(() => {
    getData(`http://localhost:8000/product_types`)
      .then((data) => {
        console.log(data.data)
        setProductTypes(data.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])
  return (
    <>
      <button className="btn btn-outline-primary mb-2" type="button">
        <CIcon icon={cibAddthis} />
      </button>
      <table className="table table-hover align-middle table-success">
        <thead>
          <tr>
            <th scope="col">TYPE ID</th>
            <th scope="col">NAME</th>
            <th scope="col">DESCRIPTION</th>
            <th scope="col" className="text-center">
              ACTION
            </th>
          </tr>
        </thead>
        <tbody>
          {productTypes.map((productType, index) => (
            <tr key={index}>
              <th scope="row">{productType._id}</th>
              <td>{productType.name}</td>
              <td>{productType.description}</td>
              <td>
                <div className="d-grid gap-2 d-md-block text-center">
                  <button className="btn btn-info " type="button">
                    <CIcon icon={cilPen} />
                  </button>
                  <button className="btn btn-danger " type="button">
                    <CIcon icon={cilDelete} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default ProductTypes
