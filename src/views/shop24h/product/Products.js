import React, { useEffect, useState } from 'react'

import { cilPen, cilDelete, cibAddthis } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
function Products() {
  const [product, setProduct] = useState([])
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions)
    const responseData = await response.json()
    return responseData
  }
  useEffect(() => {
    getData(`http://localhost:8000/products`)
      .then((data) => {
        setProduct(data.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])
  return (
    <>
      <button className="btn btn-outline-primary mb-2" type="button">
        <CIcon icon={cibAddthis} />
      </button>
      <table className="table table-hover align-middle table-success">
        <thead>
          <tr>
            <th scope="col">PRODUCT ID</th>
            <th scope="col">NAME</th>
            <th scope="col">AMOUNT</th>
            <th scope="col">BUY PRICE</th>
            <th scope="col">IMG</th>
            <th scope="col" className="text-center">
              ACTION
            </th>
          </tr>
        </thead>
        <tbody>
          {product.map((product, index) => (
            <tr key={index}>
              <th scope="row">{product._id}</th>
              <td>{product.name}</td>
              <td>{product.amount}</td>
              <td>{product.buyPrice}</td>
              <td>
                <img src={product.imageUrl} width={100} />
              </td>
              <td>
                <div className="d-grid gap-2 d-md-block text-center">
                  <button className="btn btn-info " type="button">
                    <CIcon icon={cilPen} />
                  </button>
                  <button className="btn btn-danger " type="button">
                    <CIcon icon={cilDelete} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default Products
