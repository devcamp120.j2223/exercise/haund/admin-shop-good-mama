import React, { useEffect, useState } from 'react'
import { cilPen, cilDelete, cibAddthis } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
function Customers() {
  const [customers, setCustomers] = useState([])
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions)
    const responseData = await response.json()
    return responseData
  }
  useEffect(() => {
    getData(`http://localhost:8000/customers`)
      .then((data) => {
        setCustomers(data.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])
  return (
    <>
      <button className="btn btn-outline-primary mb-2" type="button">
        <CIcon icon={cibAddthis} />
      </button>
      <table className="table table-hover align-middle table-success">
        <thead>
          <tr>
            <th scope="col">CUSTOMER ID</th>
            <th scope="col">NAME</th>
            <th scope="col">PHONE NUMBER</th>
            <th scope="col">ADDRESS</th>
            <th scope="col">EMAIL</th>
            <th scope="col">ORDER LIST</th>
            <th scope="col" className="text-center">
              ACTION
            </th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer, index) => (
            <tr key={index}>
              <th scope="row">{customer._id}</th>
              <td>{customer.fullName}</td>
              <td>{customer.phone}</td>
              <td>{customer.address}</td>
              <td>{customer.email}</td>
              <td>
                {customer.orders.map((orderId, index2) => (
                  <tr key={index2}>{orderId}</tr>
                ))}
              </td>
              <td>
                <div className="d-grid gap-2 d-md-block text-center">
                  <button className="btn btn-info " type="button">
                    <CIcon icon={cilPen} />
                  </button>
                  <button className="btn btn-danger " type="button">
                    <CIcon icon={cilDelete} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default Customers
