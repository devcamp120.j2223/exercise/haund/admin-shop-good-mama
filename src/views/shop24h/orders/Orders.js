import React, { useEffect, useState } from 'react'
import CIcon from '@coreui/icons-react'
import { cilPen, cilDelete, cibAddthis } from '@coreui/icons'
function Orders() {
  const [orders, setOrders] = useState([])
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions)
    const responseData = await response.json()
    return responseData
  }
  useEffect(() => {
    getData(`http://localhost:8000/orders`)
      .then((data) => {
        setOrders(data.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])
  return (
    <>
      <button className="btn btn-outline-primary mb-2" type="button">
        <CIcon icon={cibAddthis} />
        &ensp;Add New Order
      </button>
      <table className="table table-hover align-middle table-success">
        <thead>
          <tr>
            <th scope="col">ORDER ID</th>
            <th scope="col">PRODUCTS</th>
            <th scope="col">COST</th>
            <th scope="col">NOTE</th>
            <th scope="col" className="text-center">
              ACTION
            </th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order, index) => (
            <tr key={index}>
              <th scope="row">{order._id}</th>
              <td>
                {order.orderDetail.map((product, index2) => (
                  <tr key={index2}>
                    {product.name} x{product.quantity}
                  </tr>
                ))}
              </td>
              <td>{order.cost}</td>
              <td>{order.note}</td>
              <td>
                <div className="d-grid gap-2 d-md-block text-center">
                  <button className="btn btn-info " type="button">
                    <CIcon icon={cilPen} />
                  </button>
                  <button className="btn btn-danger " type="button">
                    <CIcon icon={cilDelete} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default Orders
