import React from 'react'

const ProductType = React.lazy(() => import('./views/shop24h/productTypes/ProductTypes'))
const Products = React.lazy(() => import('./views/shop24h/product/Products'))
const Customers = React.lazy(() => import('./views/shop24h/customers/Customers'))
const Orders = React.lazy(() => import('./views/shop24h/orders/Orders'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/shop24h/productTypes', name: 'ProductTypes', element: ProductType },
  { path: '/shop24h/product', name: 'Products', element: Products },
  { path: '/shop24h/customers', name: 'Customers', element: Customers },
  { path: '/shop24h/orders', name: 'Orders', element: Orders },
]

export default routes
